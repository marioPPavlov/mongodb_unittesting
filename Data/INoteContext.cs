﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using NotebookAppApi.Model;

namespace NotebookAppApi.Data
{
    public interface INoteContext
    {
        IMongoCollection<Note> Notes();
    }
}
